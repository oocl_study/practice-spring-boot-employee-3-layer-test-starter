package com.afs.restapi;

import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CompanyControllerTest {
    @Autowired
    MockMvc mockMvc;
    @Autowired
    CompanyRepository companyRepository;

    ObjectMapper mapper = new ObjectMapper();

    @BeforeEach
    void setUp() {
        companyRepository.clearAll();
    }

    @Test
    void should_get_all_companies_when_perform_get_given_companies() throws Exception {

        //given
        companyRepository.addCompany(new Company(1, "oocl", buildEmployeesList()));

        //when
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/companies"))
                .andReturn();

        //then
        MockHttpServletResponse response = mvcResult.getResponse();
        assertEquals(200, response.getStatus());
        List<Company> companies = mapper.readValue(response.getContentAsString(), new TypeReference<List<Company>>() {
        });
        assertEquals(1, companies.size());

        Company company = companies.get(0);
        assertEquals(1, company.getId());
        assertEquals("oocl", company.getCompanyName());
    }

    private static List<Employee> buildEmployeesList() {
        return List.of(
                new Employee(1, "Charlie", 20, "Female", 100000),
                new Employee(2, "Jaden", 20, "Man", 100000)
        );
    }
    @Test
    void should_get_all_companies_when_perform_get_given_companies_use_matchers() throws Exception {
        //given
        companyRepository.addCompany(new Company(1, "oocl", buildEmployeesList()));

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].companyName").isString())
                .andExpect(jsonPath("$[0].companyName").value("oocl"))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[0].id").value(buildEmployeesList().get(0).getId()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[0].name").value(buildEmployeesList().get(0).getName()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[0].age").value(buildEmployeesList().get(0).getAge()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[0].gender").value(buildEmployeesList().get(0).getGender()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[0].salary").value(buildEmployeesList().get(0).getSalary()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[1].id").value(buildEmployeesList().get(1).getId()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[1].name").value(buildEmployeesList().get(1).getName()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[1].age").value(buildEmployeesList().get(1).getAge()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[1].gender").value(buildEmployeesList().get(1).getGender()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[1].salary").value(buildEmployeesList().get(1).getSalary()));
        ;
    }

    @Test
    void should_return_company_when_perform_get_given_id() throws Exception {
        //given
        companyRepository.addCompany(new Company(1, "oocl", buildEmployeesList()));

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.companyName").isString())
                .andExpect(jsonPath("$.companyName").value("oocl"))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[0].id").value(buildEmployeesList().get(0).getId()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[0].name").value(buildEmployeesList().get(0).getName()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[0].age").value(buildEmployeesList().get(0).getAge()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[0].gender").value(buildEmployeesList().get(0).getGender()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[0].salary").value(buildEmployeesList().get(0).getSalary()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[1].id").value(buildEmployeesList().get(1).getId()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[1].name").value(buildEmployeesList().get(1).getName()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[1].age").value(buildEmployeesList().get(1).getAge()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[1].gender").value(buildEmployeesList().get(1).getGender()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[1].salary").value(buildEmployeesList().get(1).getSalary()));
        ;
    }


    @Test
    void should_return_employees_when_perform_get_given_company_id() throws Exception {
        //given
        companyRepository.addCompany(new Company(1, "oocl", buildEmployeesList()));

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/1/employees"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(buildEmployeesList().get(0).getId()))
                .andExpect(jsonPath("$[0].name").isString())
                .andExpect(jsonPath("$[0].name").value(buildEmployeesList().get(0).getName()))
                .andExpect(jsonPath("$[0].age").isNumber())
                .andExpect(jsonPath("$[0].age").value(buildEmployeesList().get(0).getAge()))
                .andExpect(jsonPath("$[0].gender").isString())
                .andExpect(jsonPath("$[0].gender").value(buildEmployeesList().get(0).getGender()))
                .andExpect(jsonPath("$[0].salary").isNumber())
                .andExpect(jsonPath("$[0].salary").value(buildEmployeesList().get(0).getSalary()))
                .andExpect(jsonPath("$[1].id").isNumber())
                .andExpect(jsonPath("$[1].id").value(buildEmployeesList().get(1).getId()))
                .andExpect(jsonPath("$[1].name").isString())
                .andExpect(jsonPath("$[1].name").value(buildEmployeesList().get(1).getName()))
                .andExpect(jsonPath("$[1].age").isNumber())
                .andExpect(jsonPath("$[1].age").value(buildEmployeesList().get(1).getAge()))
                .andExpect(jsonPath("$[1].gender").isString())
                .andExpect(jsonPath("$[1].gender").value(buildEmployeesList().get(1).getGender()))
                .andExpect(jsonPath("$[1].salary").isNumber())
                .andExpect(jsonPath("$[1].salary").value(buildEmployeesList().get(1).getSalary()));
        ;
    }

    @Test
    void should_return_companies_page_when_perform_get_given_companies_and_page_info() throws Exception {
        //given
        companyRepository.addCompany(new Company(1, "oocl", buildEmployeesList()));

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies?pageNumber=1&pageSize=2"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].companyName").isString())
                .andExpect(jsonPath("$[0].companyName").value("oocl"))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[0].id").value(buildEmployeesList().get(0).getId()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[0].name").value(buildEmployeesList().get(0).getName()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[0].age").value(buildEmployeesList().get(0).getAge()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[0].gender").value(buildEmployeesList().get(0).getGender()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[0].salary").value(buildEmployeesList().get(0).getSalary()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[1].id").value(buildEmployeesList().get(1).getId()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[1].name").value(buildEmployeesList().get(1).getName()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[1].age").value(buildEmployeesList().get(1).getAge()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[1].gender").value(buildEmployeesList().get(1).getGender()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[1].salary").value(buildEmployeesList().get(1).getSalary()));
        ;
    }

    @Test
    void should_update_company_when_perform_put_given_company_and_update_info() throws Exception {
        //given
        companyRepository.addCompany(new Company(1, "oocl", buildEmployeesList()));

        Company company2 = new Company(2, "carsosmart", buildEmployeesList());

        String susanJson = mapper.writeValueAsString(company2);

        //when
        mockMvc.perform(MockMvcRequestBuilders.put("/companies/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(susanJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.companyName").isString())
                .andExpect(jsonPath("$.companyName").value("carsosmart"))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[0].id").value(buildEmployeesList().get(0).getId()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[0].name").value(buildEmployeesList().get(0).getName()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[0].age").value(buildEmployeesList().get(0).getAge()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[0].gender").value(buildEmployeesList().get(0).getGender()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[0].salary").value(buildEmployeesList().get(0).getSalary()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[1].id").value(buildEmployeesList().get(1).getId()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[1].name").value(buildEmployeesList().get(1).getName()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[1].age").value(buildEmployeesList().get(1).getAge()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[1].gender").value(buildEmployeesList().get(1).getGender()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[1].salary").value(buildEmployeesList().get(1).getSalary()));

        Company company = companyRepository.getCompanies().get(0);
        assertEquals(company2.getCompanyName(), company.getCompanyName());
    }

    @Test
    void should_del_company_when_perform_del_given_companies_and_id() throws Exception {
        //given
        companyRepository.addCompany(new Company(1, "oocl", buildEmployeesList()));


        //when
        mockMvc.perform(MockMvcRequestBuilders.delete("/companies/{id}", 1));

        //then
        Assertions.assertEquals(0, companyRepository.getCompanies().size());
    }
}
