package com.afs.restapi;

import com.afs.restapi.exception.AgeErrorException;
import com.afs.restapi.exception.AgeNotMatchSalaryException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.EmployeeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.webservices.client.WebServiceClientTest;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class EmployeeServiceTest {

    private EmployeeRepository employeeRepository = mock(EmployeeRepository.class);

    private EmployeeService employeeService = new EmployeeService(employeeRepository);

    @Test
    public void should_thrown_age_exception_when_add_employee_given_employee_with_age_smaller_18_or_larger_65() {
        Employee employee1 = new Employee(1, "aa", 17, "male", 10000);
        Employee employee2 = new Employee(2, "bb", 66, "male", 10000);

        Assertions.assertThrows(AgeErrorException.class, () -> employeeService.insertEmployee(employee1));
        Assertions.assertThrows(AgeErrorException.class, () -> employeeService.insertEmployee(employee2));

        Mockito.verify(employeeRepository, times(0)).insert(any());
    }

    @Test
    public void should_thrown_age_not_match_salary_when_add_employee_given_employee_with_age_no_match_salary() {
        Employee employee = new Employee(1, "aa", 31, "male", 10000);

        Assertions.assertThrows(AgeNotMatchSalaryException.class, () -> employeeService.insertEmployee(employee));

        Mockito.verify(employeeRepository, times(0)).insert(any());
    }

    @Test
    public void should_set_employee_status_true_when_add_employee_given_employee() {
        Employee employee = new Employee(1, "aa", 20, "male", 10000);
        Employee employeeReturn = new Employee(1, "aa", 20, "male", 10000);
        employeeReturn.setStatus(true);
        when(employeeRepository.insert(any())).thenReturn(employeeReturn);

        Employee employeeSaved = employeeService.insertEmployee(employee);

        assertTrue(employeeSaved.getStatus());
        verify(employeeRepository).insert(argThat(employeeToSave -> {
            Assertions.assertTrue(employeeToSave.getStatus());
            return true;
        }));
    }

    @Test
    public void should_set_employee_status_false_when_delete_employee_given_id() {
        Employee employeeReturn = new Employee(1, "aa", 20, "male", 10000, true);
        when(employeeRepository.findById(1)).thenReturn(employeeReturn);

        employeeService.deleteEmployee(1);

        assertEquals(false, employeeReturn.getStatus());
    }

    @Test
    public void should_update_fail_when_update_employee_given_employee_with_status_false(){
        Employee employee = new Employee(1, "aa", 20, "male", 10000, false);

        employeeService.updateEmployee(1, employee);
        verify(employeeRepository, times(0)).update(1, employee);

    }
}
