package com.afs.restapi.exception;

public class ExceptionResult {
    private int code;

    private String message;

    public ExceptionResult(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
