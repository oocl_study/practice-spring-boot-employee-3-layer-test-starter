package com.afs.restapi.service;

import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.CompanyRepository;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompanyService {

    public CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public List<Company> getCompanies() {
        return companyRepository.getCompanies();
    }

    public Company getCompanyById(Integer companyId) {
        return companyRepository.getCompanies().stream()
                .filter(company -> company.getId().equals(companyId))
                .findFirst()
                .orElse(null);
    }

    public List<Employee> getEmployeesByCompanyId(Integer companyId) {
        return companyRepository.getCompanies().stream()
                .filter(company -> company.getId().equals(companyId))
                .findFirst()
                .map(company -> company.getEmployees())
                .orElse(Collections.emptyList());
    }

    public List<Company> getCompaniesByPagination(Integer pageIndex, Integer pageSize) {
        return companyRepository.getCompanies().stream()
                .skip((pageIndex - 1) * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    public void addCompany(Company company) {
        companyRepository.addCompany(company);
    }

    public Company updateCompany(Integer companyId, Company company) {
        return companyRepository.getCompanies().stream()
                .filter(storedCompany -> storedCompany.getId().equals(companyId))
                .findFirst()
                .map(storedCompany -> updateCompanyAttributes(storedCompany, company))
                .orElse(null);
    }

    private Company updateCompanyAttributes(Company companyStored, Company company) {
        if (company.getCompanyName() != null) {
            companyStored.setCompanyName(company.getCompanyName());
        }
        return companyStored;
    }

    public void remove(Integer companyId) {
        companyRepository.getCompanies().removeIf(company -> company.getId().equals(companyId));
    }
}
